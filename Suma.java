import java.util.Scanner;
 
public class main_class {
    public static void main(String[] args){
      int sum = 0, firstNumber, secondNumber;
      Scanner inputNumScanner = new Scanner(System.in);
      System.out.println("Enter First Number: ");
      firstNumber = inputNumScanner.nextInt();
 
      System.out.println("Enter Second Number: ");
      secondNumber = inputNumScanner.nextInt();
 
      sum = firstNumber + secondNumber;
 
      System.out.println("The sum of the two numbers you entered = " + sum);
    }
}